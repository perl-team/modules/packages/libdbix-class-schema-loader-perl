Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: DBIx-Class-Schema-Loader
Upstream-Contact: irc://irc.perl.org/#dbic-cabal
Source: https://metacpan.org/release/DBIx-Class-Schema-Loader

Files: *
Copyright:
 2006-2015, Caelum: Rafael Kitover <rkitover@cpan.org>
 2006-2015, Dag-Erling Smørgrav <des@des.no>
 2006-2015, Matias E. Fernandez <mfernandez@pisco.ch>
 2006-2015, SineSwiper: Brendan Byrd <byrd.b@insightcom.com>
 2006-2015, TSUNODA Kazuya <drk@drk7.jp>
 2006-2015, acmoore: Andrew Moore <amoore@cpan.org>
 2006-2015, alnewkirk: Al Newkirk <awncorp@cpan.org>
 2006-2015, andrewalker: André Walker <andre@andrewalker.net>
 2006-2015, angelixd: Paul C. Mantz <pcmantz@cpan.org>
 2006-2015, arcanez: Justin Hunter <justin.d.hunter@gmail.com>
 2006-2015, ash: Ash Berlin <ash@cpan.org>
 2006-2015, blblack: Brandon Black <blblack@gmail.com>
 2006-2015, bphillips: Brian Phillips <bphillips@cpan.org>
 2006-2015, btilly: Ben Tilly <btilly@gmail.com>
 2006-2015, domm: Thomas Klausner <domm@plix.at>
 2006-2015, gugu: Andrey Kostenko <a.kostenko@rambler-co.ru>
 2006-2015, hobbs: Andrew Rodland <arodland@cpan.org>
 2006-2015, ilmari: Dagfinn Ilmari MannsE<aring>ker <ilmari@ilmari.org>
 2006-2015, jhannah: Jay Hannah <jay@jays.net>
 2006-2015, jnap: John Napiorkowski <jjn1056@yahoo.com>
 2006-2015, kane: Jos Boumans <kane@cpan.org>
 2006-2015, mattp: Matt Phillips <mattp@cpan.org>
 2006-2015, mephinet: Philipp Gortan <philipp.gortan@apa.at>
 2006-2015, moritz: Moritz Lenz <moritz@faui2k3.org>
 2006-2015, mst: Matt S. Trout <mst@shadowcatsystems.co.uk>
 2006-2015, mstratman: Mark A. Stratman <stratman@gmail.com>
 2006-2015, oalders: Olaf Alders <olaf@wundersolutions.com>
 2006-2015, rbo: Robert Bohne <rbo@cpan.org>
 2006-2015, rbuels: Robert Buels <rbuels@gmail.com>
 2006-2015, ribasushi: Peter Rabbitson <ribasushi@cpan.org>
 2006-2015, schwern: Michael G. Schwern <mschwern@cpan.org>
 2006-2015, spb: Stephen Bennett <spb@exherbo.org>
 2006-2015, timbunce: Tim Bunce <timb@cpan.org>
 2006-2015, waawaamilk: Nigel McNie <nigel@mcnie.name>
License: Artistic or GPL-1+

Files: debian/*
Copyright: 2006-2009, Krzysztof Krzyzaniak (eloy) <eloy@debian.org>
 2008-2024, gregor herrmann <gregoa@debian.org>
 2010-2011, Jonathan Yu <jawnsy@cpan.org>
 2012-2015, Dominique Dumont <dod@debian.org>
License: Artistic or GPL-1+

License: Artistic
 This program is free software; you can redistribute it and/or modify
 it under the terms of the Artistic License, which comes with Perl.
 .
 On Debian systems, the complete text of the Artistic License can be
 found in `/usr/share/common-licenses/Artistic'.

License: GPL-1+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 1, or (at your option)
 any later version.
 .
 On Debian systems, the complete text of version 1 of the General
 Public License can be found in `/usr/share/common-licenses/GPL-1'.
