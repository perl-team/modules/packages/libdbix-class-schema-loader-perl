libdbix-class-schema-loader-perl (0.07052-1) unstable; urgency=medium

  * Import upstream version 0.07052.
  * Update years of packaging copyright.
  * Declare compliance with Debian Policy 4.6.2.
  * Set upstream metadata fields: Bug-Database, Bug-Submit.

 -- gregor herrmann <gregoa@debian.org>  Sat, 20 Jan 2024 19:58:00 +0100

libdbix-class-schema-loader-perl (0.07051-1) unstable; urgency=medium

  * Team upload

  [ Laurent Baillet ]
  * fix lintian file-contains-trailing-whitespace warning

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Bump debhelper from old 10 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Repository, Repository-Browse.
  * Remove obsolete fields Contact, Name from debian/upstream/metadata
    (already present in machine-readable debian/copyright).

  [ Florian Schlichting ]
  * Import upstream version 0.07051.
  * Re-add (build-)dependency on String::CamelCase
  * Drop obsolete (build-)dependencies leftover by janitor
  * Add Rules-Requires-Root: no
  * Declare compliance with Debian Policy 4.6.1
  * Annotate build-dependencies with nocheck

 -- Florian Schlichting <fsfs@debian.org>  Sat, 26 Nov 2022 22:05:45 +0100

libdbix-class-schema-loader-perl (0.07049-1) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * Import upstream version 0.07049.
  * debian/copyright: remove paragraph referencing dropped files.
  * Update (build) dependencies.
  * Declare compliance with Debian Policy 4.1.4.

 -- gregor herrmann <gregoa@debian.org>  Thu, 19 Apr 2018 21:13:22 +0200

libdbix-class-schema-loader-perl (0.07048-1) unstable; urgency=medium

  * Import upstream version 0.07048.
  * Drop 0001-Disable-cloning-when-merging-hashes.patch
    (fixed differently upstream).
  * Add new build and runtime dependency on libcurry-perl.

 -- gregor herrmann <gregoa@debian.org>  Mon, 22 Jan 2018 17:45:31 +0100

libdbix-class-schema-loader-perl (0.07047-2) unstable; urgency=medium

  [ Damyan Ivanov ]
  * declare conformance with Policy 4.1.3 (no changes needed)

  [ gregor herrmann ]
  * Add patch 0001-Disable-cloning-when-merging-hashes.patch to handle
    Hash::Merge's changed cloning behaviour.
    Thanks to Niko Tyni for writing the patch. (Closes: #882618)
  * Update years of packaging copyright.
  * Bump debhelper compatibility level to 10.

 -- gregor herrmann <gregoa@debian.org>  Sat, 20 Jan 2018 17:41:58 +0100

libdbix-class-schema-loader-perl (0.07047-1) unstable; urgency=medium

  * Team upload

  [ Alex Muntada ]
  * Remove inactive pkg-perl members from Uploaders.

  [ Damyan Ivanov ]
  * New upstream version 0.07047
  * declare conformance with Policy 4.1.1

 -- Damyan Ivanov <dmn@debian.org>  Sun, 29 Oct 2017 18:14:15 +0000

libdbix-class-schema-loader-perl (0.07046-1) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * Remove Jonathan Yu from Uploaders. Thanks for your work!
  * Import upstream version 0.07046.
  * Drop spelling.patch, merged upstream.
  * Drop version constraints from (build) dependencies which are already
    satisfied in oldstable. Thanks to cme.
  * Declare compliance with Debian Policy 3.9.8.

 -- gregor herrmann <gregoa@debian.org>  Sun, 11 Sep 2016 18:33:43 +0200

libdbix-class-schema-loader-perl (0.07045-1) unstable; urgency=medium

  * Import upstream version 0.07045
  * Update years of packaging copyright.
  * Update list of upstream copyright holders and copyright years.
  * Make some build dependencies versioned like their runtime
    counterparts.
  * Add a patch to fix a spelling mistake in the POD.

 -- gregor herrmann <gregoa@debian.org>  Fri, 22 Jan 2016 23:00:06 +0100

libdbix-class-schema-loader-perl (0.07043-2) unstable; urgency=medium

  * control: use alternate dependency to require Sub::Util (Tx ntyni)

 -- Dominique Dumont <dod@debian.org>  Mon, 28 Dec 2015 09:31:52 +0100

libdbix-class-schema-loader-perl (0.07043-1) unstable; urgency=medium

  [ gregor herrmann ]
  * Update {Upstream-,}Contact in debian/{copyright,upstream/metadata}.
  * Rename autopkgtest configuration file(s) as per new pkg-perl-
    autopkgtest schema.

  [ Dominique Dumont ]
  * Imported Upstream version 0.07043
  * control: updated std-version to 3.9.6
  * control: depends on perl 5.22 for Sub::Util module

 -- Dominique Dumont <dod@debian.org>  Sun, 27 Dec 2015 21:22:23 +0100

libdbix-class-schema-loader-perl (0.07042-2) unstable; urgency=medium

  * Team upload.

  [ gregor herrmann ]
  * debian/tests/control: add stanza for new runtime-deps-and-recommends
    tests.
  * Drop debian/tests/control, add Testsuite field to debian/control
    instead.

  [ Niko Tyni ]
  * Fix autopkgtest failures.

 -- Niko Tyni <ntyni@debian.org>  Fri, 26 Sep 2014 21:07:49 +0300

libdbix-class-schema-loader-perl (0.07042-1) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ gregor herrmann ]
  * Add debian/upstream/metadata
  * Imported upstream version 0.07042
  * Add autopkgtest control file.

 -- gregor herrmann <gregoa@debian.org>  Sun, 07 Sep 2014 17:04:14 +0200

libdbix-class-schema-loader-perl (0.07041-1) unstable; urgency=medium

  * New upstream release.
  * Update build dependencies.

 -- gregor herrmann <gregoa@debian.org>  Tue, 12 Aug 2014 23:36:03 +0200

libdbix-class-schema-loader-perl (0.07040-1) unstable; urgency=medium

  * New upstream release.
  * debian/copyright: add new upstream copyright holders.

 -- gregor herrmann <gregoa@debian.org>  Thu, 29 May 2014 19:46:42 +0200

libdbix-class-schema-loader-perl (0.07039-1) unstable; urgency=medium

  * New upstream release.
  * Update years of packaging copyright, add new copyright holder.

 -- gregor herrmann <gregoa@debian.org>  Tue, 14 Jan 2014 19:37:22 +0100

libdbix-class-schema-loader-perl (0.07038-1) unstable; urgency=medium

  * New upstream release.
  * Update years of packaging copyright.

 -- gregor herrmann <gregoa@debian.org>  Fri, 27 Dec 2013 21:32:54 +0100

libdbix-class-schema-loader-perl (0.07037-1) unstable; urgency=low

  * Team upload

  * Imported Upstream version 0.07037
  * reverse order of libpod-simple-perl (>= 3.22) | perl (>= 5.17.3)
    build-dependency
  * drop trailing slash from metacpan URLs
  * claim conformance with Policy 3.9.5

 -- Damyan Ivanov <dmn@debian.org>  Wed, 06 Nov 2013 23:22:20 +0200

libdbix-class-schema-loader-perl (0.07036-1) unstable; urgency=low

  [ gregor herrmann ]
  * debian/control: update {versioned,alternative} (build) dependencies.

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Dominique Dumont ]
  * Imported Upstream version 0.07036. Check NEWS for behavior changes
  * compat: bumped to 9
  * copyright: updated years
  * control: updated dependencies according to upstream changes

 -- Dominique Dumont <dod@debian.org>  Thu, 08 Aug 2013 17:02:48 +0200

libdbix-class-schema-loader-perl (0.07025-1) unstable; urgency=low

  * New upstream release.

 -- gregor herrmann <gregoa@debian.org>  Sat, 09 Jun 2012 15:09:33 +0200

libdbix-class-schema-loader-perl (0.07024-1) unstable; urgency=low

  * New upstream release.

 -- gregor herrmann <gregoa@debian.org>  Sat, 26 May 2012 21:36:41 +0200

libdbix-class-schema-loader-perl (0.07023-1) unstable; urgency=low

  * New upstream release.
  * Add versioned (build) dependency on libclass-inspector-perl.

 -- gregor herrmann <gregoa@debian.org>  Mon, 07 May 2012 18:16:56 +0200

libdbix-class-schema-loader-perl (0.07022-1) unstable; urgency=low

  * New upstream release.
  * debian/copyright: add new upstream copyright holder.

 -- gregor herrmann <gregoa@debian.org>  Mon, 09 Apr 2012 19:17:36 +0200

libdbix-class-schema-loader-perl (0.07021-1) unstable; urgency=low

  * New upstream release.
  * Add build dependency on libhash-merge-perl.

 -- gregor herrmann <gregoa@debian.org>  Sat, 07 Apr 2012 14:21:31 +0200

libdbix-class-schema-loader-perl (0.07019-1) unstable; urgency=low

  * new upstream release
  * spelling patch: applied upstream, removed
  * copyright: refreshed copyright years for 2012
  * control: updated standards-version

 -- Dominique Dumont <dod@debian.org>  Fri, 30 Mar 2012 18:10:47 +0200

libdbix-class-schema-loader-perl (0.07017-1) unstable; urgency=low

  * New upstream release.
  * debian/copyright: add new upstream copyright holders, bump years of
    packaging copyright.
  * Add a patch to fix a spelling mistake.

 -- gregor herrmann <gregoa@debian.org>  Wed, 08 Feb 2012 22:53:08 +0100

libdbix-class-schema-loader-perl (0.07015-1) unstable; urgency=low

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ Salvatore Bonaccorso ]
  * debian/copyright: Replace DEP5 Format-Specification URL from
    svn.debian.org to anonscm.debian.org URL.

  [ gregor herrmann ]
  * New upstream release.
  * debian/copyright: update list of copyright holders, update years of
    copyright for inc/Module/*.
  * Update build and runtime dependencies.
  * New upstream releases 0.07014, 0.07015.

  [ Dominique Dumont ]
  * control:
    + added /me to uploaders.
    + Added new dependencies (libmoosex-markasmethods-perl
      libstring-toidentifier-en-perl)
    + added build-dep on libdbix-class-introspectablem2m-perl
  * copyright: added /me in debian files

 -- Dominique Dumont <dod@debian.org>  Wed, 01 Feb 2012 14:09:35 +0100

libdbix-class-schema-loader-perl (0.07010-2) unstable; urgency=low

  * Add (build) dependency on libcarp-clan-perl (closes: #633742).
  * Set Standards-Version to 3.9.2 (no changes).
  * Add perl (>= 5.11.1) as an alternative build dependency for Test::More.

 -- gregor herrmann <gregoa@debian.org>  Fri, 15 Jul 2011 16:56:19 +0200

libdbix-class-schema-loader-perl (0.07010-1) unstable; urgency=low

  * New upstream release
  * Update dependencies per upstream
  * Bump to debhelper 8
  * Refresh copyright information

 -- Jonathan Yu <jawnsy@cpan.org>  Mon, 21 Mar 2011 18:50:23 -0400

libdbix-class-schema-loader-perl (0.07002-1) unstable; urgency=low

  [ gregor herrmann ]
  * New upstream release.
  * debian/copyright: add new copyright holder, point to
    /usr/share/common-licenses/GPL-1.
  * Set Standards-Version to 3.9.1 (no changes).
  * debian/control: remove libdata-dumper-concise-perl, add libtry-tiny-perl;
    new Recommends: libmoose-perl, libmoosex-nonmoose-perl

  [ Jonathan Yu ]
  * New upstream release

  [ gregor herrmann ]
  * debian/control: remove libclass-c3-perl, bump
    libclass-accessor-grouped-perl, add libnamespace-clean-perl, add
    libtest-warn-perl, add libmro-compat-perl.

 -- gregor herrmann <gregoa@debian.org>  Sun, 14 Nov 2010 16:52:29 +0100

libdbix-class-schema-loader-perl (0.07000-1) unstable; urgency=low

  [ Jonathan Yu ]
  * New upstream release 0.06000
  * Use new 3.0 (quilt) source format
  * Update dependencies per upstream

  [ gregor herrmann ]
  * New upstream release 0.06001.
  * New upstream release 0.07000.
  * (Build) depend on perl >= 5.10.1 (for Exporter 5.63).
  * Update build dependencies (test suite).
  * Update list of upstream copyright holders.
  * Add (build) dependency on liblingua-en-inflect-phrase-perl.

 -- gregor herrmann <gregoa@debian.org>  Sun, 06 Jun 2010 22:10:38 +0200

libdbix-class-schema-loader-perl (0.05003-2) unstable; urgency=low

  * Add build and runtime dependency on liblist-moreutils-perl
    (closes: #573661).

 -- gregor herrmann <gregoa@debian.org>  Sat, 13 Mar 2010 13:03:00 +0100

libdbix-class-schema-loader-perl (0.05003-1) unstable; urgency=low

  * New upstream release

 -- Jonathan Yu <jawnsy@cpan.org>  Sat, 20 Feb 2010 20:38:16 -0500

libdbix-class-schema-loader-perl (0.05002-1) unstable; urgency=low

  * New upstream release
  * Add dependency on namespace::autoclean per upstream

 -- Jonathan Yu <jawnsy@cpan.org>  Mon, 15 Feb 2010 18:45:41 -0500

libdbix-class-schema-loader-perl (0.05001-1) unstable; urgency=low

  [ Jonathan Yu ]
  * New upstream release
  * Rewrite control description
  * Drop unnecessary dependency on Module::Install
  * Use new short debhelper rules format
  * Add myself to Uploaders and Copyright
  * Add dependency on File::Slurp per upstream

  [ Krzysztof Krzyżaniak (eloy) ]
  * New upstream release
  * debian/control:
   - libclass-unload-perl added to dependencies

  [ gregor herrmann ]
  * Update several build and runtime dependencies according to META.yml.
  * Enable backward compatibility tests.
  * debian/copyright: add additional copyright holders.
  * Add /me to Uploaders.

 -- gregor herrmann <gregoa@debian.org>  Tue, 09 Feb 2010 17:09:04 +0100

libdbix-class-schema-loader-perl (0.04006-1) unstable; urgency=low

  * New upstream release
  * convert package to debhelper7, update debian/control and debian/rules
  * debian/control: Update Standards-Version to 3.8.1
  * debian/copyright: Update to new schema

 -- Krzysztof Krzyżaniak (eloy) <eloy@debian.org>  Mon, 04 May 2009 11:15:04 +0200

libdbix-class-schema-loader-perl (0.04005-2) UNRELEASED; urgency=low

  * debian/control: Changed: Switched Vcs-Browser field to ViewSVN
    (source stanza).

 -- gregor herrmann <gregoa@debian.org>  Sun, 16 Nov 2008 20:41:47 +0100

libdbix-class-schema-loader-perl (0.04005-1) unstable; urgency=low

  * New upstream release
  * debian/control:
   + added headers from Debian Perl Group
   + Standards-Version updated to 3.7.3 (no changes)
  * debian/copyright: updated with new upstream link and copyright

 -- Krzysztof Krzyżaniak (eloy) <eloy@debian.org>  Wed, 09 Apr 2008 11:08:36 +0200

libdbix-class-schema-loader-perl (0.04004-1) unstable; urgency=low

  * New upstream release

 -- Krzysztof Krzyzaniak (eloy) <eloy@debian.org>  Tue, 20 Nov 2007 15:21:10 +0100

libdbix-class-schema-loader-perl (0.04003-1) unstable; urgency=low

  * New upstream release

 -- Krzysztof Krzyzaniak (eloy) <eloy@debian.org>  Tue, 09 Oct 2007 13:59:38 +0200

libdbix-class-schema-loader-perl (0.04002-1) unstable; urgency=low

  * New upstream release

 -- Krzysztof Krzyzaniak (eloy) <eloy@debian.org>  Fri, 27 Jul 2007 15:25:12 +0200

libdbix-class-schema-loader-perl (0.04001-1) unstable; urgency=low

  * New upstream release

 -- Krzysztof Krzyzaniak (eloy) <eloy@debian.org>  Fri, 29 Jun 2007 09:07:05 +0200

libdbix-class-schema-loader-perl (0.04000-1) unstable; urgency=low

  * New upstream release
  * converted for using libmodule-install-perl

 -- Krzysztof Krzyzaniak (eloy) <eloy@debian.org>  Fri, 08 Jun 2007 10:36:12 +0200

libdbix-class-schema-loader-perl (0.03012-1) unstable; urgency=low

  * New upstream release

 -- Krzysztof Krzyzaniak (eloy) <eloy@debian.org>  Tue, 22 May 2007 19:05:46 +0200

libdbix-class-schema-loader-perl (0.03011-1) unstable; urgency=low

  * New upstream release

 -- Krzysztof Krzyzaniak (eloy) <eloy@debian.org>  Mon, 16 Apr 2007 19:31:07 +0200

libdbix-class-schema-loader-perl (0.03010-1) unstable; urgency=low

  * New upstream release (closes: #417785)

 -- Krzysztof Krzyzaniak (eloy) <eloy@debian.org>  Sat, 07 Apr 2007 12:43:08 +0200

libdbix-class-schema-loader-perl (0.03009-1) unstable; urgency=low

  * New upstream release
  * debian/control: added liblingua-en-inflect-number-perl: to dependencies

 -- Krzysztof Krzyzaniak (eloy) <eloy@debian.org>  Fri,  1 Dec 2006 16:29:33 +0100

libdbix-class-schema-loader-perl (0.03008-1) unstable; urgency=low

  * New upstream release

 -- Krzysztof Krzyzaniak (eloy) <eloy@debian.org>  Fri, 20 Oct 2006 21:39:36 +0200

libdbix-class-schema-loader-perl (0.03007-1) unstable; urgency=low

  * New upstream release

 -- Krzysztof Krzyzaniak (eloy) <eloy@debian.org>  Fri, 28 Jul 2006 12:49:38 +0200

libdbix-class-schema-loader-perl (0.03005-1) unstable; urgency=low

  * New upstream release
  * debian/compat: increased to 5

 -- Krzysztof Krzyzaniak (eloy) <eloy@debian.org>  Wed, 19 Jul 2006 18:53:41 +0200

libdbix-class-schema-loader-perl (0.03004-1) unstable; urgency=low

  * New upstream release

 -- Krzysztof Krzyzaniak (eloy) <eloy@debian.org>  Tue, 11 Jul 2006 13:11:05 +0200

libdbix-class-schema-loader-perl (0.03003-1) unstable; urgency=low

  * New upstream release
  * debian/source.lintian-overrides: added
  * not uploaded - wait for libdata-dump-perl

 -- Krzysztof Krzyzaniak (eloy) <eloy@debian.org>  Tue,  6 Jun 2006 10:28:00 +0200

libdbix-class-schema-loader-perl (0.03000-1) unstable; urgency=low

  * New upstream release
  * debian/control:
   - Standards-Version: increased to 3.7.2 without additional changes

 -- Krzysztof Krzyzaniak (eloy) <eloy@debian.org>  Tue, 30 May 2006 15:49:39 +0200

libdbix-class-schema-loader-perl (0.02007-1) unstable; urgency=low

  * New upstream release

 -- Krzysztof Krzyzaniak (eloy) <eloy@debian.org>  Thu, 23 Mar 2006 11:19:25 +0100

libdbix-class-schema-loader-perl (0.02006-1) unstable; urgency=low

  * New upstream release
  * debian/control:
   - Build-Depends-Indep: libtest-pod-perl and libtest-pod-coverage-perl added

 -- Krzysztof Krzyzaniak (eloy) <eloy@debian.org>  Fri, 17 Mar 2006 14:26:20 +0100

libdbix-class-schema-loader-perl (0.02005-1) unstable; urgency=low

  * New upstream release (closes: #354827)

 -- Krzysztof Krzyzaniak (eloy) <eloy@debian.org>  Wed,  1 Mar 2006 11:06:11 +0100

libdbix-class-schema-loader-perl (0.02004-1) unstable; urgency=low

  * New upstream release

 -- Krzysztof Krzyzaniak (eloy) <eloy@debian.org>  Tue, 28 Feb 2006 10:51:20 +0100

libdbix-class-schema-loader-perl (0.02000-1) unstable; urgency=low

  * Initial Release.

 -- Krzysztof Krzyzaniak (eloy) <eloy@debian.org>  Mon, 13 Feb 2006 14:09:04 +0100
